# Setup
Globally install nodemon to watch your files status
```
npm install nodemon -g
```

Install node modules
```
npm install
```

##### Be shure to have a mysql server running with the correct database and user. You'll find the requests in the database.sql file.

# Run
Launch the server using nodemon...
```
nodemon app.js
```

...or
```
npm start
```

# References and documentation

express: https://expressjs.com/fr/4x/api.html

how to use nodejs/ejs/express/mysql: https://dev.to/achowba/build-a-simple-app-using-node-js-and-mysql-19me