CREATE DATABASE  IF NOT EXISTS `websql` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `websql`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: websql
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(45) NOT NULL DEFAULT 'Default category',
  `title` varchar(150) NOT NULL DEFAULT 'Default title',
  `subtitle` varchar(200) NOT NULL DEFAULT 'Default subtitle',
  `content` mediumtext,
  `main_picture_name` varchar(200) NOT NULL DEFAULT 'Default main picture name',
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tags` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;

INSERT INTO `articles` VALUES (1,'lifestyle','Gaming and Entertainment','How to have fun in Korea ? ','<p>One thing you might notice in Korea is how much Korean love going out and have any type of activities outside. For example, you will easily find several Karaoke wherever you are in Seoul or crowded chicken and beer on Friday night. If going out on weekend night is international, going out to play video game is more a Korean thing.</p><p>Indeed, there is almost as much karaoke room as place called PC Bang. PC Bang are places where you can rent high quality computer for gaming. Usually the room are quite dark and you can hear multiple game sound around you. It’s cheap and really convenient if you want to play without having to pay for a gaming computer.</p><p>Also, for the love of Koreans towards gaming, you can also find several arcades games in Seoul. It’s cheap as well and you can have a good time alone or with friends playing the different games; from claws machine to Mario Kart or horror 4D games.</p>','images/gaming.jpg','2018-12-16 16:11:20','game life entertainment'),
(2,'lifestyle','Hiking in South Korea','Favorite Korean pastime','Hiking is a favourite pastime for South Koreans, and many flock to the country’s perfectly maintained trails on weekends to escape the big smoke. The trailheads are usually easily accessed by public transport, and for most day hikes you won’t even need a map.</br></br>The scenery is spectacular, as is the South Korean ‘alpine fashion’ you will encounter. You will see most of your fellow South Korean hikers sporting brand new fluorescent hiking gear that is almost as remarkable as the trails themselves. Hiking is a great way to meet South Koreans, and you will probably be stopped for an entertaining selfie or two during your hikes.','images/hiking.jpg','2018-11-04 16:10:08','mountain hiking landscape'),
(3,'food','Chicken & Beer','Korean classic  ','In South Korea the most common restaurant is Chicken & Beer. Many students, employees or foreigners will eat there. Chicken & Beer is such a success thanks to sharing, which is an important concept in Korean culture. So you share a dish of fried chicken and a beer in a friendly place. In addition the Chicken & Beer is really cheap because you share a meal and you also share the bill.</br></br>You have a lot of different style of chicken, with bone or boneless, with different sort of sauce like curry, spicy, soy. It is very pleasant to go there for the evening.</br></br>Up:</br><ul><li>Groups</li><li>Cheap</li><li>Friendly</li></ul></br>Down</br><ul><li>Only Chicken</li></ul>','images/Chicken&Beer.jpg','2018-12-10 12:10:20','chicken beer'),
(4,'visit','The War Memorial of Korea','Best museum','The War Memorial of Korea was established to remember the Korean War and to symbolize the desire for peaceful reunification of Korea. The museum consists of 6 individual exhibition halls and a combat experience room, a screening room and an outdoor exhibition area with memorial statues.</br></br>The Statue of Brothers located in the south-west corner of the outdoor exhibition area at the museum symbolizes the Korean War and the hope for reunification. The statue depicts a scene where a family’s oldest son, a South Korean soldier and his younger brother, a North Korean soldier meet on a battlefield. Their embrace expresses reconciliation, love and forgiveness.</br></br>There is an extensive garden area around the memorial with an artificial waterfall, and a lake where visitors can picnic whilst enjoying the beautiful landscape. The museum houses more than 10,000 artifacts in the indoor and outdoor exhibition areas. It takes visitors about three hours to complete the tour. The first floor exhibition hall features a history of war from prehistoric times up to the Korean War. The combat experience room on the third floor offers visitors a special opportunity to experience a simulated life and death situation on the battlefield through special effects.','images/war_museum.jpg','2018-11-20 20:10:20','war korea'),
(6,'studentlife','Discovering Korean Village','Chung-Ang helps us to discover Korea','<p>As a student in Chung-Ang University, we have during each semester several activities we can realize with the school in order to discover more about Korea. During the fall semester, I did one of them and had the chance to go to a village and learn about traditional aspect of Korea.</p><p>I learnt how is made the tofu traditionally and tried it myself. I then eat it with some makgeolli. Since the tofu was warm and the makgeolli was cold it was really nice to eat together. At lunch we had some bibimbap with rice and whole grains and a lot of vegetables. It was really good since it has been made with their own vegetables!</p><p>I also learnt some traditional Korean game while being there. It was funny and interesting to discover those game. Some reminded me of some French game we have. It was nice to see that even being thousands of kilometers away from each other, France and Korea share some simple things.</p><p>At the end of the day, we made some rice cake with wooden hammer and then ate them with bean powder. It was really good ! </p>','images/ricecake.jpg','2018-12-16 16:22:08','folk village discover'),
(7,'food','Tteokbokki','Julie s favourite korean food','<p>I think my favorite Korean food would be Tteokbokki. I discovered this snack eight years ago at the same time I was discovering Korean pop and Korean drama. I slowly gained interest into trying it, but it was impossible to find where I was living. I think in France you might be able to taste it in Paris and only Paris. And I’m not even sure you can. I remember asking my dad who came to Korea five days for work if I did try it or not. I was disappointed when he told me no, he didn’t even know what I was talking about. I had that Korean friend whom I met on an online website. I asked her how it was, and she told me it was delicious, a bit spicy but sweet and Koreans loved it.</p><p>As time passes, I grew up and moved with my boyfriend in a bigger city than the one where my parent lived (this city is Nantes). I was freer about what I ate and what I bought. I usually went once every two months to a shop called “Indochine” where you can buy all kind of Asian food and condiment. I was frequently checking their Facebook page, searching for something new to try. One day, they put a picture with tteokbokki sauce with “new” written on it. I remember rushing to the shop to get one pot before it’s sold out. I managed to get two; one mild spicy and one spicy. However, I didn’t find the right tteok in the shop. The didn’t have the cylinder shape, only the sliced one. Since it was better than nothing, I took it and cooked it for dinner. I tried the mild spicy one and it was way too much spicy for me. I loved the taste and the texture, but the spices were too much to handle for me. I drank a one-liter bottle of drinking yogurt that night. I didn’t even try the spicy one and throw it away. I was kind of disappointed. Not about the tteokbokki, but about me, to not be able to eat it.</p><p>Later the same year, the same shop began selling already-made tteokbokki in small boxes. You just had to open the tteok, but the sauce on it and microwave it. It was a bit chewier than the previous one but still good. I managed to eat it entirely. I was getting excited cause at that time, I already knew I was going to South Korea. </p><p>Finally, we arrive at the tteokbokki in Korea. I only ate those at the cafeteria for now. The first one was too spicy for me, I had to eat rice with it to calm my tongue (yes, rice with rice). Slowly I’m getting used to the spice and this lunch I had it without rice and I managed to finish it entirely. The next step is going outside to eat some. I didn’t go yet, because my Koreans friend told me they are even spicier outside and I’m not sure I can handle it. But I will try because I really love the taste and I will miss it a lot back in France.</p>','images/tteokbokki.jpg','2018-12-16 15:14:20','food korea'),
(8,'visit','MyeongDong','Where fashion and street food meet','<p>One of the places I found interesting in Korea is the Myeongdong area. It might seem like any other busy district, but it appears to be really different from anything I know in France since you can see so much of the city in only one place.<p><p>First, you can go shopping. There is plenty shop on the outside, either for clothes or cosmetic. But the magic happens as well in the underground with multiple stores selling from souvenirs to electronic stuff. As far as I know, we don’t have any underground market in France while it seems to be really common in Korea, especially Seoul.</p><p>Then, when you’re craving for some food after your shopping, you can try some delicious food without even searching for a restaurant. Myeongdong is THE place to get street food. I personally really like the bread egg and get one whenever I go there. All the smells might make you crazy and want to try everything you see. Street food is seeable everywhere in Seoul but you’ll Myeongdong is one of those places where you have so much choice that you end with a stomach more than full. In France, we don’t really have street food, you can only see sometimes some people eating sandwich outside of the bakery or sometimes some van selling crepes once a week.</p><p>Third, is all the café you can find there. Some “normal” café as well as some crazy café with animal such as cat, dog or even raccoon. Something really unique I’ve only seen in Japan before.</p><p>And finally, I love the landscape you have at Myeongdong. You can see the Namsan Tower as well as big building and it becomes beautiful at night.</p><p>In conclusion, I think Myeongdong is an interesting place cause only going there can give you a good idea of how Koreans live nowadays: shopping, street food, cosmetic and coffee. That is probable my favorite place in Seoul.</p>','images/myeongdong.jpg','2018-12-16 15:44:20','fashion food');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thoughts`
--

DROP TABLE IF EXISTS `thoughts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `thoughts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL DEFAULT 'Default name',
  `title` varchar(150) NOT NULL DEFAULT 'Default title',
  `content` mediumtext,
  `image_path` varchar(150) NOT NULL DEFAULT '/images/person_1.jpg',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thoughts`
--

LOCK TABLES `thoughts` WRITE;
/*!40000 ALTER TABLE `thoughts` DISABLE KEYS */;
INSERT INTO `thoughts` VALUES (1,'Leo','My first experience abroad','<p>Some content</p><p>another line</p><p>Hey we can use html tags in our content !</p>','/images/leo.jpg'),
(2,'Julie','Discovering Korea','<p>I have been a huge fan of Korea and Korean pop when I was 13. Going to Korea was a dream at that time but I did not had the chance to. As time passed by, my interest toward k-pop decreased, but I kept some love the country.  When I had to choose in which school I wanted to study,  I ended up choosing the one I am in because we had to do a year abroad. Since I always wanted to live somewhere else in the world than France, it was a great opportunity.</p><p>I finally chose Korea for my year abroad, it was kind of an evidence. Weirdly, when I arrived in Korea, I felt like I knew already a lot of things due to everything I was on the internet. But still, discovering Seoul was amazing. The life here is amazing. I never felt safer being in the street at night, everything seems cheap compared to France and I love how Koreans entertained themselves. I definitely do not regret my choice.</p><p>However, I do not see myself ending up my life here. It is a really good country to study but I think working in Korea would be too hard for me.</p>','/images/julie.JPG'),
(3,'Sebastien','A very fancy title','Very fancy content','/images/seb.jpg');
/*!40000 ALTER TABLE `thoughts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-06 11:08:58
