const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const path = require('path');
const app = express();

const {getHomePage} = require('./routes/index');
const {getAboutPage} = require('./routes/about');
const {getArticlePage} = require('./routes/article');
const {getContactPage} = require('./routes/contact');
const {getCategoryPage} = require('./routes/category');
const {getThoughtPage} = require('./routes/thought');
const {getSearchPage} = require('./routes/search');

const port = '5000'

// create connection to database
const db = mysql.createConnection ({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'websql'
});

// connect to database
db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database');
});
global.db = db;

// configure middleware
app.set('port', process.env.port || port); // set express to use this port
app.set('views', __dirname + '/views'); // set express to look in this folder to render our view
app.set('view engine', 'ejs'); // configure template engine
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); // parse form data client
app.use(express.static(path.join(__dirname, 'public'))); // configure express to use public folder

// routes for the app
app.get('/', getHomePage);
app.get('/about', getAboutPage);
app.get('/article', getArticlePage);
app.get('/contact', getContactPage);
app.get('/category', getCategoryPage);
app.get('/thought', getThoughtPage);
app.get('/search', getSearchPage);

// set the app to listen on the port
app.listen(port, () => {
    console.log(`Server running on port: ${port}`);
});
