module.exports = {
    getSearchPage: function (req, res) {
        var tags = req.query.tags;

        if (tags != undefined && /^[a-zA-Z]+$/.test(tags)) {

            db.query("SELECT * FROM `articles` WHERE `tags` LIKE '%" + tags + "%' ORDER BY creation_date ASC", (err, result) => {
                if (err) {
                    console.error("invalid db request, query [" + query + "]");
                    res.redirect('/');
                }
                articlesTag = result;
                db.query("SELECT * FROM `articles` ORDER BY id ASC", (err, result) => {
                    if (err) {
                        console.error("invalid db request, query [" + query + "]");
                        res.redirect('/');
                    }
                res.render('pages/search', {
                    title: "Search results",
                    articlesTag: articlesTag,
                    articles : result,
                    tag: tags
                })
            });
            });
        }
        else {
            res.redirect('/');
        }
    }
};