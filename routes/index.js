
function getRandomCarouselArticles(articles) {
    var randomCarouselElementsArray = new Array();
    var articleAlradyPushed = new Array();

    for (var i = 0; i<3;){
        var nb = Math.floor(Math.random() * articles.length) + 1;
        if (!articleAlradyPushed.includes(nb)){
            articleAlradyPushed.push(nb);
            randomCarouselElementsArray.push(articles[nb-1]);
            i++;
        }
    }
    return randomCarouselElementsArray;
}

module.exports = {
    getHomePage: function (req, res) {
        //let query = 

        db.query("SELECT (SELECT COUNT(*) FROM articles WHERE category LIKE 'Lifestyle') AS Lifestyle, (SELECT COUNT(*) FROM articles WHERE category LIKE 'Food') AS Food, (SELECT COUNT(*) FROM articles WHERE category LIKE 'StudentLife') AS StudentLife, (SELECT COUNT(*) FROM articles WHERE category LIKE 'visit') AS Visit", (err, result) => {
            if (err) {
                console.error("invalid db request");
                res.redirect('/');
            }
            var nbArticle = result;
            db.query("SELECT * FROM `articles` ORDER BY id DESC", (err, result) => {
                if (err) {
                    console.error("invalid db request");
                    res.redirect('/');
                }
                var carouselElements = getRandomCarouselArticles(result);
                res.render('pages/index', {
                    title: "The Land of Morning Calm",
                    nbArticle: nbArticle,
                    carouselArticles: carouselElements,
                    articles: result
                })
            })
        });
    }
}   