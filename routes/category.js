module.exports = {
    getCategoryPage: function (req, res) {
        var type = req.query.type;

        // il aurait fallu créer une table représentant les category avec leur nom simplifié et leur nom affiché
        if (type == undefined || !type.match(/^(lifestyle|food|studentlife|visit)$/)) {
            console.error("invalid category type, type [" + type + "]")
            res.redirect('/');
        }
        else {
            let query = "SELECT * FROM `articles` WHERE `category` LIKE '" + type + "' ORDER BY creation_date ASC";

            db.query(query, (err, result) => {
                if (err) {
                    console.error("invalid db request, query [" + query + "]");
                    res.redirect('/');
                }
                // il aurait fallu créer une table représentant les category avec leur nom simplifié et leur nom affiché
                switch (type) {
                    case "lifestyle":
                        type = "Lifestyle";
                        break;
                    case "food":
                        type = "Food";
                        break;
                    case "studentlife":
                        type = "Student Life";
                        break;
                    case "visit":
                        type = "Visit";
                        break;
                    default:
                        break;
                }
                articlesCategory = result;
                db.query("SELECT * FROM `articles` ORDER BY id ASC", (err, result) => {
                    if (err) {
                        console.error("invalid db request, query [" + query + "]");
                        res.redirect('/');
                    }
                res.render('pages/category', {
                    title: "Category",
                    type: type,
                    articlesCategory: articlesCategory,
                    articles: result
                })
            });
            });
        }
    }
};