module.exports = {
    getAboutPage: function(req, res) {
        let query = "SELECT * FROM `articles` ORDER BY id ASC";
        db.query(query, (err, result) => {
            if (err) {
                console.error("invalid db request, query [" + query + "]");
                res.redirect('/');
            }      
        res.render('pages/about', {
        title: "About",
        articles: result})
        });
    }
};