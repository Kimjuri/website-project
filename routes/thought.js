module.exports = {
    getThoughtPage: function (req, res) {
        var name = req.query.name;

        if (name == undefined || !name.match(/^(Leo|Julie|Sebastien)$/)) {
            console.error("invalid name, name [" + name + "]")
            res.redirect('/');
        }
        else {
            db.query("SELECT * FROM `thoughts` WHERE `name` = '" + name + "' LIMIT 1", (err, result) => {
                if (err) {
                    console.error("invalid db request, query [" + query + "]");
                    res.redirect('/');
                }
                thought = result;
                db.query("SELECT * FROM `articles` ORDER BY id ASC", (err, result) => {
                    if (err) {
                        console.error("invalid db request, query [" + query + "]");
                        res.redirect('/');
                    }
                res.render('pages/thought', {
                    title: "Our thoughts",
                    name: name,
                    thought: thought,
                    articles: result
                })
            });
            });
        }
    }
};